const fs = require('fs');
const SimpleSitemapGenerator = require('simple-sitemap-generator');

new SimpleSitemapGenerator('https://www.araujoysegovia.com', {

    /**
     * @param {Object} item
     */
    onFetchComplete: function (item) {
        if ( item.loc == 'https://www.araujoysegovia.com/' ) {
            item.priority = 0.8;
        }
        else if ( item.loc.indexOf('/2017-18/') != -1 ) {
            item.priority = 0.8;
        }
        else {
            item.priority = 0.5;
        }

        console.log( 'FETCH: ' +item.loc+ ' -> priority:' +item.priority );
    },

    /**
     * @param {String} sitemap
     */
    onComplete: function (sitemap) {
        fs.writeFile('./sitemap.xml', sitemap, 'utf-8')
    },
    maxConcurrency: 10,
    exclude: [ 'gif', 'ico', 'bmp', 'ogg', 'webp', 'mp4', 'webm', 'mp3', 'ttf', 'woff', 'json', 'rss', 'atom', 'gz', 'zip', 'rar', '7z', 'css', 'js', 'gzip', 'exe', 'svg' ]
})
